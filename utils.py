from keras.preprocessing.image import ImageDataGenerator
from skimage import transform
from sklearn.utils import shuffle

def transformation(x_train, y_train):
    datagen = ImageDataGenerator(
                                 rotation_range=16,
                                 zoom_range=[0.9, 1.],
                                 height_shift_range=0.10,
                                 shear_range=0.15
    )

    return datagen

def sliding_window(img, patch_size=(100, 100),
                   istep=4, jstep=4, scale=1.0):
    Ni, Nj = (int(scale * s) for s in patch_size)
    for i in range(0, img.shape[0] - Ni, istep):
        for j in range(0, img.shape[1] - Ni, jstep):
            patch = img[i:i + Ni, j:j + Nj, :]
            if scale != 1:
                patch = transform.resize(patch, patch_size)
            yield [i, j], patch

def shuffle_data(x, y):
    x, y = shuffle(x, y)
    return x, y

