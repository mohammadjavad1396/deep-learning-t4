import os
import re

import cv2
import matplotlib.pyplot as plt
import numpy as np


def prepare_data():
    path = 'NWPU VHR-10 dataset'
    images = []
    targets = []

    for img_number_string in range(1, len(os.listdir(path + os.sep + 'positive image set')) + 1):
        img_path = path + os.sep + 'positive image set' + os.sep \
                   + str('{0:0=3d}'.format(img_number_string)) + '.jpg'
        target_path = path + os.sep + 'ground truth' + os.sep \
                      + str('{0:0=3d}'.format(img_number_string)) + '.txt'

        with open(target_path) as target_file:
            image = cv2.imread(img_path)

            for line in target_file:
                line = re.sub(r"([()])", "", line)
                if line.strip():
                    lst = list(map(int, line.strip().split(',')))
                    min_bbox, max_bbox, idx = (lst[0], lst[1]), (lst[2], lst[3]), lst[4]
                    reshaped_img = cv2.resize(image[min_bbox[1]:max_bbox[1], min_bbox[0]:max_bbox[0], :],
                                              dsize=(100, 100), interpolation=cv2.INTER_CUBIC)

                    images.append(reshaped_img)  # add positive images to list
                    targets.append(idx)  # add positive labels to list
        # try:
        if int(img_number_string) < 151:
            negative_path = path + os.sep + 'negative image set' + os.sep \
                            + str('{0:0=3d}'.format(img_number_string)) + '.jpg'
            negative_image = cv2.imread(negative_path)
            negative_reshape = cv2.resize(negative_image,
                                          dsize=(100, 100), interpolation=cv2.INTER_CUBIC)
            images.append(negative_reshape)  # add negative images list till images exist
            targets.append(11)  # add negative labels to list till images exist

            # negative_image_lst = [negative_image[0:int(negative_image.shape[0] / 4), :, :],
            #                       negative_image[int(negative_image.shape[0] / 4):int(negative_image.shape[0] / 2), :,
            #                       :],
            #                       negative_image[2 * int(negative_image.shape[0] / 4):3 * int(negative_image.shape[0] / 2),
            #                       :, :],
            #                       negative_image[3 * int(negative_image.shape[0] / 4):, :, :]]
            # for i in range(len(negative_image_lst)):
            #     negative_reshape = cv2.resize(negative_image_lst[i],
            #                                   dsize=(100, 100), interpolation=cv2.INTER_CUBIC)
            #     images.append(negative_reshape)  # add negative images list till images exist
            #     targets.append(11)  # add negative labels to list till images exist

    return np.array(images, dtype='float16') / 255, np.array(targets, dtype='float16')


a, b = prepare_data()
plt.hist(b, bins='auto')
plt.show()
print(b)
