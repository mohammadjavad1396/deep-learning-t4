import os

import cv2
import numpy as np
import tensorflow as tf
from skimage.transform import pyramid_gaussian

from utils import sliding_window

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
tf.config.experimental.set_memory_growth(physical_devices[0], True)

path = "NWPU VHR-10 dataset\\positive image set\\001.jpg"

img = cv2.imread(path)


def image_division(image):
    lst_patch = []
    lst_indices = []
    resized_pics = []
    print(image.shape)
    # print(type(image), type(image[0, 0, 0]))
    if image.shape[0] < 3000 and image.shape[1] < 3000:
        for (i, resized) in enumerate(pyramid_gaussian(image, downscale=1.2)):
            if resized.shape[0] < 700 or resized.shape[1] < 700:
                break
            else:
                indices, patches = zip(*sliding_window(resized, scale=.7))
                print(type(patches))
                lst_indices.append(indices)
                lst_patch.append(patches)
                resized_pics.append(resized)

    lst_patch = np.array([lst_patch], dtype='float16')
    lst_indices = np.array([lst_indices])
    resized_pics = np.array([resized_pics])

    return lst_indices, lst_patch, resized_pics
