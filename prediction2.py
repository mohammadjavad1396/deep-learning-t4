import os

import cv2
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

from custom_net import RESNET_LIKE
from utils import sliding_window

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
tf.config.experimental.set_memory_growth(physical_devices[0], True)


def prediction(image_path, model_path):
    img = cv2.imread(image_path)
    model = RESNET_LIKE(num_classes=11)
    model.built = True
    model.call(np.random.randn(32, 100, 100, 3))
    model.load_weights(model_path)
    scale = [.5, .75, 1]  # scale in hyperparameter and for each image is different
    fig, ax = plt.subplots()
    ax.imshow(img, cmap='gray')
    ax.axis('off')
    save_argument = []
    boxes = []
    scores = []

    # temp = int(img.shape[0] / 2)

    # lst_img = [img[0:temp, :, :], img[temp:, :, :]]

    # if memmory error happand it has to use this line except below line

    lst_img = [img]
    for i in range(len(scale)):
        Ni, Nj = 100 * scale[i], 100 * scale[i]
        for k in range(
                len(lst_img)):  # this for is for because memmory error. if memmory errord, lst_img and temp must be use

            # print(lst_img[k])

            lst_indices, lst_patches = zip(*sliding_window(lst_img[k].astype('float32'), scale=scale[i]))

            # if k == 1:
            #     lst_indices = np.array(lst_indices)
            #     lst_indices[:, 0] += temp

            patches = np.array([patch / 255 for patch in lst_patches])

            print('hello world')

            predictions = model.predict(patches)

            for j, preds in enumerate(predictions):
                if np.argmax(preds) != 10 and np.max(preds) > .8:
                    boxes.append([lst_indices[j][0], lst_indices[j][1], lst_indices[j][0] + Nj, lst_indices[j][1] + Ni])
                    scores.append(np.max(preds))
                    save_argument.append(np.argmax(preds) + 1)

    save_argument = np.array(save_argument)
    boxes = np.array(boxes, dtype='float16')
    scores = np.array(scores, dtype='float16')

    indices = tf.image.non_max_suppression_with_scores(boxes, scores, max_output_size=40, iou_threshold=.2,
                                                       soft_nms_sigma=.6)

    for index in indices[0]:
        print(boxes[index][0], boxes[index][1])
        ax.add_patch(
            plt.Rectangle((boxes[index][1], boxes[index][0]), Nj, Ni, edgecolor='red',
                          alpha=0.3, lw=2, facecolor='none'))
        plt.text(boxes[index][1], boxes[index][0],
                 str(save_argument[index]), fontsize=15)  # str(save_argument[index]),

    plt.show()


model_path = 'F:\\checkpoints\\weights-improvement-49-0.0121-bigger'
img_path = "NWPU VHR-10 dataset\\positive image set\\001.jpg"
prediction(img_path, model_path)
