import numpy as np
from sklearn.utils import class_weight


def weights_class(train_labels):
    train_labels = np.argmax(train_labels, axis=1) + 1
    weight = class_weight.compute_class_weight('balanced', np.unique(train_labels), train_labels)
    weight = {i: weight[i] for i in range(11)}
    return weight

