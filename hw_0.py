import os
import re

import cv2
import matplotlib.pyplot as plt


def visual_dataset(img_number_string):
    img_number_string = str('{0:0=3d}'.format(int(img_number_string)))
    path = 'NWPU VHR-10 dataset'
    img_path = path + os.sep + 'positive image set' + os.sep + str(img_number_string) + '.jpg'
    target_path = path + os.sep + 'ground truth' + os.sep + str(img_number_string) + '.txt'
    img = cv2.imread(img_path)
    font = cv2.FONT_HERSHEY_SIMPLEX
    font_scale = 1
    with open(target_path, 'r') as target_file:
        for line in target_file:
            line = re.sub(r"([()])", "", line)
            lst = list(map(int, line.split(',')))
            min_bbox, max_bbox, idx = (lst[0], lst[1]), (lst[2], lst[3]), lst[4]
            bottomLeftCornerOfText = min_bbox
            cv2.rectangle(img, min_bbox, max_bbox, (0, 255, 0), 2)
            cv2.putText(img, str(idx), bottomLeftCornerOfText, font, font_scale, (0, 255, 0), 2)

    plt.imshow(img)
    plt.show()


visual_dataset(36)
