import os

import tensorflow as tf

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
tf.config.experimental.set_memory_growth(physical_devices[0], True)

import numpy as np
from keras.callbacks import ModelCheckpoint
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder  # this function is used to prepare one-hot encoded labels
import keras

from custom_net import RESNET_LIKE
from hw2_1 import prepare_data
from hw2_3 import weights_class
from utils import transformation, shuffle_data
from datetime import datetime


def train_net():
    filepath = "F:" + os.sep + "checkpoints" + os.sep + \
               "weights-improvement-{epoch:02d}-{loss:.4f}-bigger"

    logdir = 'F:' + os.sep + 'model' + os.sep + 'logs' + os.sep + 'scalars' + \
             os.sep + datetime.now().strftime("%Y%m%d-%H%M%S")

    if 'checkpoints' not in os.listdir('F:'):
        os.mkdir('F:' + os.sep + 'checkpoints')

    checkpoint = ModelCheckpoint(
        filepath, monitor='loss',
        verbose=0,
        save_best_only=True,
        mode='min'
    )

    X, y = prepare_data()

    X_shuff, y_shuff = shuffle_data(X, y)
    y_shuff = np.expand_dims(np.array(y_shuff), axis=1)
    encoder = OneHotEncoder(sparse=False)
    encoder.fit(y_shuff)
    y_shuff = encoder.transform(y_shuff)
    X_train, X_val, y_train, y_val = train_test_split(X_shuff, y_shuff,
                                                      test_size=0.1, random_state=22)

    class_weights = weights_class(y_train)
    print(class_weights)
    datagen = transformation(X_train, y_train)
    datagen.fit(X_train)

    model = RESNET_LIKE(num_classes=11)
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
    callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=30)
    callbacks_list = [checkpoint, tensorboard_callback, callback]
    model.compile(
        optimizer=keras.optimizers.Adam(learning_rate=1e-5),
        loss=keras.losses.CategoricalCrossentropy(),
        metrics=['accuracy'])
    model.model().summary()
    model.fit(datagen.flow(X_train, y_train), batch_size=32
              , epochs=70, validation_data=(X_val, y_val), callbacks=callbacks_list,
              class_weight=class_weights)


if __name__ == '__main__':
    train_net()
